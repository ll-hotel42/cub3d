/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ll-hotel <ll-hotel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/06/26 15:59:10 by ll-hotel          #+#    #+#             */
/*   Updated: 2024/06/26 17:38:20 by ll-hotel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H
# include "libft.h"
# include <sys/types.h>
# include <stdbool.h>

enum	e_tile
{
	EMPTY = 0x00,
	WALL = 0x02,
	FLOOR = 0x01,
	NORTH = 'N',
	EAST = 'E',
	SOUTH = 'S',
	WEST = 'W',
	ERROR = 0xff,
};

typedef struct s_map	t_map;
struct	s_map
{
	t_fptr	*array;
	u_long	height;
	u_long	width;
};

t_map	*cub_parser(char *file);
bool	valid_map(t_map *map);

void	map_free(t_map *map);
#endif
