/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ll-hotel <ll-hotel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/06/26 16:48:28 by ll-hotel          #+#    #+#             */
/*   Updated: 2024/06/26 17:44:28 by ll-hotel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static bool	check_bytes(t_fptr *row, u_long row_i, bool *found_floor);
static bool	check_walls(t_map *map);
static char	convert(int c);
static bool	check_surroundings(t_map *map, u_long row, u_long col);

bool	valid_map(t_map *map)
{
	u_long	row;
	bool	found_floor;

	if (!map || !map->array)
		return (false);
	found_floor = false;
	map->width = map->array[0].len;
	row = 0;
	while (row < map->height)
	{
		map->width = maxul(map->array[row].len, map->width);
		if (!check_bytes(&map->array[row], row, &found_floor))
			return (false);
		row += 1;
	}
	if (!found_floor)
	{
		ft_dprintf(2, "Map does not have floor\n");
		return (false);
	}
	return (check_walls(map));
}

static bool	check_bytes(t_fptr *row, u_long row_i, bool *found_floor)
{
	char	c;
	u_long	i;

	i = 0;
	while (i <= row->len)
	{
		c = row->ptr[i];
		if (c == '0')
			*found_floor = true;
		if (c && c != '\n' && c != ' ' && c != '1' && c != '0' && \
				c != NORTH && c != EAST && c != SOUTH && c != WEST)
		{
			ft_dprintf(2, "Unexpected byte [%d]`%c' at line %u, col %u\n", \
					c, c, row_i, i);
			return (false);
		}
		row->ptr[i] = convert(c);
		i += 1;
	}
	return (true);
}

static bool	check_walls(t_map *map)
{
	u_long	row_i;
	u_long	col_i;

	row_i = -1;
	while (++row_i < map->height)
	{
		col_i = -1;
		while (++col_i < map->array[row_i].len)
		{
			if ((map->array[row_i].ptr[col_i] == EMPTY || \
					map->array[row_i].ptr[col_i] == FLOOR) && \
					check_surroundings(map, row_i, col_i) == false)
			{
				ft_dprintf(2, "Wall issue at line %d, col %d\n", row_i, col_i);
				return (false);
			}
		}
	}
	return (true);
}

static char	convert(int c)
{
	if (c == ' ' || c == '\n' || !c)
		c = EMPTY;
	else if (ft_isdigit(c))
		c = FLOOR + c - '0';
	return (c);
}

static bool	check_surroundings(t_map *map, u_long row, u_long col)
{
	const char	tile = map->array[row].ptr[col];
	char		up;
	char		left;
	char		right;
	char		down;

	up = EMPTY;
	if (row > 0 && col < map->array[row - 1].len)
		up = map->array[row - 1].ptr[col];
	down = EMPTY;
	if (row + 1 < map->height && col < map->array[row + 1].len)
		down = map->array[row + 1].ptr[col];
	left = EMPTY;
	if (col > 0)
		left = map->array[row].ptr[col - 1];
	right = EMPTY;
	if (col + 1 < map->array[row].len)
		right = map->array[row].ptr[col + 1];
	if (tile == EMPTY)
		return (up != FLOOR && down != FLOOR && \
				left != FLOOR && right != FLOOR);
	return (up != EMPTY && down != EMPTY && \
			left != EMPTY && right != EMPTY);
}
