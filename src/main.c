#include "cub3d.h"
#include <stdio.h>

int	main(int argc, char **argv)
{
	if (argc < 2)
		return 1;

	t_map	*map = cub_parser(argv[1]);
	if (!map)
		return 1;
	for (u_long c,r = 0; r < map->height; r += 1)
	{
		for (c = 0; c < map->array[r].len; c += 1)
			printf("%d ", map->array[r].ptr[c]);
		printf("\n");
	}
	map_free(map);
}
