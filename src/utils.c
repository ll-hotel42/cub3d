/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ll-hotel <ll-hotel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/06/26 15:58:07 by ll-hotel          #+#    #+#             */
/*   Updated: 2024/06/26 16:42:03 by ll-hotel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <stdlib.h>

void	map_free(t_map *map)
{
	u_long	i;

	if (map && map->array)
	{
		i = 0;
		while (i < map->height)
			free(map->array[i++].ptr);
		free(map->array);
	}
	if (map)
		free(map);
}
