/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ll-hotel <ll-hotel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/06/26 15:16:28 by ll-hotel          #+#    #+#             */
/*   Updated: 2024/06/26 17:38:12 by ll-hotel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "libft.h"
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static t_map	*read_fd(int fd);

t_map	*cub_parser(char *file)
{
	t_map	*map;
	int		fd;

	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		ft_dprintf(2, "Could not open `%s': %s\n", file, strerror(errno));
		return (NULL);
	}
	map = read_fd(fd);
	close(fd);
	if (valid_map(map) == false)
	{
		ft_dprintf(2, "Invalid map `%s'\n", file);
		map_free(map);
		map = NULL;
	}
	return (map);
}

static t_map	*read_fd(int fd)
{
	t_map	*map;
	t_vec	map_rows;
	t_fptr	fptr;

	map = ft_calloc(1, sizeof(*map));
	if (!map)
		return (NULL);
	vec_new(&map_rows, sizeof(*map->array));
	fptr.ptr = get_next_line(fd);
	while (fptr.ptr)
	{
		fptr.len = ft_strlen(fptr.ptr);
		fptr.len -= (fptr.len > 1 && fptr.ptr[fptr.len - 1] == '\n');
		if (!vec_addback(&map_rows, &fptr))
		{
			vec_clear(&map_rows, free);
			perror("Parser");
			return (NULL);
		}
		map->height += 1;
		fptr.ptr = get_next_line(fd);
	}
	map->array = map_rows.array;
	return (map);
}
