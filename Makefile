CC = cc

SRC_DIR = src
INC_DIR = include
OBJ_DIR = .obj
LFT_DIR = libft

CFLAGS = -Wall -Wextra -Werror
IFLAGS = -I${INC_DIR} -I${LFT_DIR}
DFLAGS = -MMD -MP
LFLAGS = -L${LFT_DIR} -lft

OBJS = $(patsubst %.c,${OBJ_DIR}/%.o,\
	   main.c \
	   parsing.c \
	   valid_map.c \
	   utils.c \
	   )
DEPS = ${OBJS:.o=.d}

LIBFT = ${LFT_DIR}/libft.a
NAME = cube

.PHONY: all
all: ${NAME}

${NAME}: ${LIBFT} ${OBJS}
	${CC} ${IFLAGS} ${DFLAGS} -o $@ ${OBJS} ${LFLAGS}

.SILENT: ${LIBFT}
.PHONY: ${LIBFT}
${LIBFT}:
	make --no-print-directory -C ${LFT_DIR}

${OBJ_DIR}/%.o: ${SRC_DIR}/%.c | ${OBJ_DIR}
	${CC} ${IFLAGS} ${DFLAGS} -o $@ -c $<

${OBJ_DIR}:
	mkdir -p $(sort $(dir ${OBJS}))

.PHONY: clean
clean:
	@make --no-print-directory -C ${LFT_DIR} clean
	rm -rf ${OBJ_DIR}

.PHONY: fclean
fclean: clean
	@make --no-print-directory -C ${LFT_DIR} fclean
	rm -f ${NAME}

.SILENT: re
.PHONY: re
re: fclean
	make --no-print-directory
